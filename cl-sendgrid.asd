;;;; cl-sendgrid.asd

(asdf:defsystem #:cl-sendgrid
  :serial t
  :description "Describe cl-sendgrid here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:drakma
               #:yason
               #:flexi-streams)
  :components ((:file "package")
               (:file "cl-sendgrid")))

