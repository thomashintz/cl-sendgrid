(require 'drakma) (require 'yason) (require 'flexi-streams)

(defpackage #:cl-sendgrid
  (:use #:cl #:drakma #:yason)
  (:export #:send-email #:sendgrid-error #:sendgrid-error-message))

