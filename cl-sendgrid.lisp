(in-package :cl-sendgrid)

(define-condition sendgrid-error (error)
  ((sendgrid-error-message :initarg :sendgrid-error-message :reader sendgrid-error-message)))

(defun send-email (&key api-user api-key subject to text html from reply-to from-name)
  (let ((r (parse (flexi-streams:octets-to-string
                   (http-request "https://sendgrid.com/api/mail.send.json"
                                 :method :post
                                 :parameters `(("api_user" . ,api-user)
                                               ("api_key" . ,api-key)
                                               ("subject" . ,subject)
                                               ("to" . ,to)
                                               ("replyto" . ,reply-to)
                                               ("fromname" . ,from-name)
                                               ("text" . ,text)
                                               ("html" . ,html)
                                               ("from" . ,from))))
                  :object-as :alist)))
    (if (string-equal (cdr (assoc "message" r :test #'string-equal)) "success")
        t
        (error 'sendgrid-error :sendgrid-error-message
               (cdr (assoc "errors" r :test #'string-equal))))))
